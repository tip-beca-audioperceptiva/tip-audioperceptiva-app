extends Node2D

var topLeft = Vector2()
var topRight = Vector2()
var bottomLeft = Vector2()
var bottomRight = Vector2()

func _ready():
	pass # Replace with function body.


func _draw():
	var points_arc = PoolVector2Array()
	points_arc.push_back(to_local(topLeft))
	points_arc.push_back(to_local(topRight))
	points_arc.push_back(to_local(bottomRight))
	points_arc.push_back(to_local(bottomLeft))
	var colors = PoolColorArray([Color.black, Color.black, Color.black, Color.black])
	draw_polygon(points_arc, colors)
