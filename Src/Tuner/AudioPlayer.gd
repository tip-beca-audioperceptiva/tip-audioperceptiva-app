extends Node2D

onready var notePlayer = $NotePlayer
var audio_file
var sfx
var defNotas
	
func _ready():
	Events.connect(Events.player_changed_note, self, "_on_Player_change_note")
	Events.connect(Events.player_finished, self, "stop")
	defNotas = {
		"Do3": load("res://Notas/Notas_melodia/Do3.ogg"),
		"Do4": load("res://Notas/Notas_melodia/Do4.ogg"),
		"Mi3": load("res://Notas/Notas_melodia/Mi3.ogg"),
		"Mi4": load("res://Notas/Notas_melodia/Mi4.ogg"),
		"Sol2": load("res://Notas/Notas_melodia/Sol2.ogg"),
		"Sol3": load("res://Notas/Notas_melodia/Sol3.ogg"),
		"Sol4": load("res://Notas/Notas_melodia/Sol4.ogg"),
		"Silencio": load("")
	}

func _on_Player_change_note(note, duration):
	notePlayer.stream = defNotas[note]
	notePlayer.play()

func stop():
	notePlayer.stop()
