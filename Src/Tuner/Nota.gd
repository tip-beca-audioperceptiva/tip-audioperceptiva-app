extends Node2D

onready var figura
var blancaTexture
var blancaSilencioTexture
var negraSilencioTexture
var notePosition
onready var corchea = false
onready var tope = $tope
onready var grosor = $grosor
var duration 
var bpm
var partituraPosition
var altoRenglon
var prevNote
var cantDecimales 
var tempo 
var negraTempo
var corcheaTempo

func _ready():
	figura = $figura
	blancaTexture = load("res://Assets/Sprites/Notas/blanca.png")
	blancaSilencioTexture = load("res://Assets/Sprites/Notas/sBlanca.png")
	negraSilencioTexture  = load("res://Assets/Sprites/Notas/sNegra.png")
	notePosition = $NotePosition
	corregirFigura()
	corregirIdentacion()
	ligarCorcheas()
	
func init(duration, bpm, partituraPosition, altoRenglon, prevNote):
	self.duration = duration
	self.bpm = bpm
	self.partituraPosition = partituraPosition
	self.altoRenglon = altoRenglon
	self.prevNote = prevNote
	cantDecimales = 0.1
	tempo = stepify(bpm, cantDecimales)
	negraTempo = stepify(60/tempo, cantDecimales)
	corcheaTempo = stepify(negraTempo/2, cantDecimales)
	
func corregirFigura():
	if negraTempo < duration:
		figura.texture = blancaTexture
	elif corcheaTempo == duration:
		corchea = true
		
func ligarCorcheas():
	if deboUnirCorcheas():
		conectarNota()
	
func conectarNota():
	var ligadura = $ligadura
	ligadura.topLeft = prevNote.tope.global_position
	ligadura.bottomLeft = prevNote.grosor.global_position
	ligadura.topRight = self.tope.global_position
	ligadura.bottomRight = self.grosor.global_position
	ligadura.update()
	corchea = false
	

func corregirIdentacion():
	if partituraPosition < 3:
		figura.position.y += altoRenglon*-0.5
		tope.position.y   += altoRenglon*-1
		grosor.position.y += altoRenglon*-1
	if partituraPosition == 5:
		$figura/renglon.show()
	if partituraPosition == 6:
		$figura/renglon.show()
		var renglonSuperior = $figura/renglonSuperior
		renglonSuperior.show()
		renglonSuperior.position.y -= 180
	draw_silence(partituraPosition)
	
	if (deboUnirCorcheas() && prevNote.partituraPosition < 3 || !deboUnirCorcheas() && partituraPosition <3):
		girar()

func deboUnirCorcheas():
	return (prevNote && prevNote.corchea) && corchea

func girar():
#	La función gira todos los elementos
	var ajusteLigaduraOffset = altoRenglon*-1.5
	figura.set_flip_h(true)
	figura.set_flip_v(true)
	figura.offset = figura.offset * -1
	tope.position = tope.position * -1
	grosor.position = grosor.position * -1
	tope.position.x = +10
	grosor.position.x = +10
	if partituraPosition <3:
		tope.position.y += ajusteLigaduraOffset
		grosor.position.y += ajusteLigaduraOffset 

func draw_silence(position):
	if position == 7:
		if negraTempo < duration:
			figura.texture = blancaSilencioTexture
		else:
			figura.texture = negraSilencioTexture
		$figura.position.y -= altoRenglon*4
