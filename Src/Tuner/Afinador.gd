extends Node2D

var nota
var finish 
enum relacion {GRAVE = -1, AFINADA = 0, AGUDA = 1}
var segundosAfinando = 0
signal correctTuning(tiempoAfinado, delta)
signal incorrectTuning(tuning)

func _ready():
	Events.connect(Events.player_changed_note, self, "_on_Player_change_note")
	Events.connect(Events.player_finished, self, "_on_Player_finish")
	finish = true

func _process(delta):
	if(!finish && !nota == "Silencio"):
		var tuning = Tuner.tuner(nota);
		if(tuning==relacion.AFINADA):
			segundosAfinando += delta
			emit_signal("correctTuning", segundosAfinando, delta)
		elif(tuning):
			segundosAfinando = 0
			emit_signal("incorrectTuning", tuning)

func _on_Player_change_note(newNote, duration):
	nota = newNote

func _on_Player_finish():
	finish = true

func startTuning():
	finish = false

func stopTuning():
	finish = true
