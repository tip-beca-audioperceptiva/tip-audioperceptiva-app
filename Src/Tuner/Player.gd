extends Node2D

func _ready():
	$Partitura/Compas.texture = load("res://Assets/Sprites/Notas/"+JsonParser.getTimeSignature()+".png")

func initExercise():
	$Partitura.initExercise()

func finishExersice():
	$Partitura.finishExersice()
