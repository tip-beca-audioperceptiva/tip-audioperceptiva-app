extends Node2D

var cantNotas = 1
export var anchoNota = 150
var altoRenglon = 20
var espacioSuperior = 200
var progresoNota = 0
var indiceNotaActual = 0
var Nota = preload("res://Src/Tuner/Nota.tscn")
var speed = 1
var finish = true
var exersice

func _process(delta):
	if !finish:
		_play_exersice(delta)
	
func _play_exersice(delta):
	progresoNota -= delta
	if progresoNota <= 0:
		indiceNotaActual += 1
		if indiceNotaActual >= exersice.size():
			finishExersice()
			return
		var duration = exersice[indiceNotaActual].duration
		progresoNota += duration
		changeNote(indiceNotaActual, progresoNota)
		speed = 1/duration
	self.position.x -= speed * delta * anchoNota
	
func _ready():
	exersice = JsonParser.getExercise()
	var exersice_bpm = JsonParser.getBpm()
	cantNotas += exersice.size()
	progresoNota = exersice[0].duration
	speed = 1/progresoNota
	var contadorNotas = 1
	var prevNote = null
	for n in exersice:
		var nota = Nota.instance()
		nota.init(n.duration, exersice_bpm, n.note, altoRenglon, prevNote)
		nota.position.x = anchoNota * contadorNotas
		nota.position.y = espacioSuperior+altoRenglon * n.note
		self.add_child(nota)
		contadorNotas += 1
		prevNote = nota

func _draw():
	dibujar_pentagrama()

func finishExersice():
	Events.send_player_finished()
	finish = true
	self.position.x = 0
	progresoNota = exersice[0].duration
	speed = 1/progresoNota
	
func dibujar_pentagrama():
	var altura = espacioSuperior
	draw_line(Vector2(0,altura), Vector2(cantNotas*anchoNota, altura), Color.black)
	altura += altoRenglon
	draw_line(Vector2(0,altura), Vector2(cantNotas*anchoNota, altura), Color.black)
	altura += altoRenglon
	draw_line(Vector2(0,altura), Vector2(cantNotas*anchoNota, altura), Color.black)
	altura += altoRenglon
	draw_line(Vector2(0,altura), Vector2(cantNotas*anchoNota, altura), Color.black)
	altura += altoRenglon
	draw_line(Vector2(0,altura), Vector2(cantNotas*anchoNota, altura), Color.black)
	
func changeNote(indiceNotaActual, progresoNota):
	var nota = exersice[indiceNotaActual].name
	Events.send_player_changed_note(nota, progresoNota)

func initExercise():
	self.position.x = 0
	indiceNotaActual = 0
	progresoNota = exersice[0].duration
	speed = 1/progresoNota
	changeNote(0, progresoNota)
	finish = false
