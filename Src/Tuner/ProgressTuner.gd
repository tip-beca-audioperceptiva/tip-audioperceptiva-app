extends ProgressBar

func _ready():
	Events.connect(Events.player_changed_note, self, "_on_Player_change_note")
	Events.connect(Events.player_finished, self, "_on_Player_finish")

func _on_Player_change_note(note, progresoNota):
	value = 0
	max_value = progresoNota * 0.9

func _on_Afinador_correctTuning(tiempoAfinado, delta):
	value += delta

func _on_Player_finish():
	value = 0
