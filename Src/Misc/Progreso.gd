extends Node2D

var center = Vector2(200, 200)
var radius = 80
var angle_from = 0
var angle_to = 0
var color = Color(0.0, 1.0, 0.0)

func draw_circle_arc(center, radius, angle_from, angle_to, color):
	var nb_points = 32
	var points_arc = PoolVector2Array()
	for i in range(nb_points + 1):
		var angle_point = deg2rad(angle_from + i * (angle_to-angle_from) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	for index_point in range(nb_points):
		draw_line(points_arc[index_point], points_arc[index_point + 1], color, 20)
		
func _draw():
	draw_circle_arc(center, radius, angle_from, angle_to, color)
