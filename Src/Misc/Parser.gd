extends Node

#func _ready():
#	var a = getExercise(1)
#	print(NotesManager.getMusicSheetIndex("C3"))

func getExercise():
	var ejercicio = []
	var json_result = getJson()
	for n in Array(json_result["tracks"][0]["notes"]):
		ejercicio.push_back(Dictionary(n))
	return _patchExercise(ejercicio)
	
func getBpm():
	var json_result = getJson()
	return int(json_result["header"]["tempos"][0]["bpm"])
	
func getTimeSignature():
	var json_result = getJson()
	var compas = json_result["header"]["timeSignatures"][0]["timeSignature"]
	return str(compas[0])+"x"+str(compas[1])
	
func getJson():
	var file = File.new()
	var bpm = "" if !str(Events.actualBpmIndex) else "-"+str(Events.actualBpmIndex)+"bpm"
	var e = Events.actualExersiceIndex
	var filename = "/"+Events.actualExersiceIndex+bpm+".json"
	file.open("res://Exercises/"+Events.actualExersiceIndex+filename, file.READ)
	var json = file.get_as_text()
	var json_result = JSON.parse(json).result
	file.close()
	return json_result
	
func _patchExercise(exercise):
	var patchedExercice = []
	var tiempoTotal = 0
	for row in exercise:
		row.duration = stepify(row.duration, 0.1)
		row.time = stepify(row.time, 0.1)
		var diferencia = row.time - tiempoTotal
		if (diferencia > 0.2):
			var silenceDuration = row.time - tiempoTotal
			patchedExercice.push_back(_addSilence(silenceDuration, tiempoTotal))
			tiempoTotal += silenceDuration
		patchedExercice.push_back(_patchNote(row)) 
		tiempoTotal += row.duration
	return patchedExercice

func _patchNote(row):
	row.erase("durationTicks")
	row.erase("ticks")
	row.erase("velocity")
	row["note"] = NotesManager.getMusicSheetIndex(row["name"])
	row["name"] = NotesManager.translate(row["name"])
	return row
	
#duration:0.6, midi:48, name:Do3, time:0
func _addSilence(duration, time):
	var silence = {}
	silence["duration"] = duration
	silence["name"] = "Silencio"
	silence["time"] = time
	silence["note"] = 7
	return silence
