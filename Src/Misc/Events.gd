extends Node

var actualExersiceIndex
var actualBpmIndex

const player_finished = "player_finished"
signal player_finished()
func send_player_finished():
	emit_signal(player_finished)

const player_changed_note = "player_changed_note"
signal player_changed_note(note, progress)
func send_player_changed_note(note, progress):
	emit_signal(player_changed_note, note, progress)
