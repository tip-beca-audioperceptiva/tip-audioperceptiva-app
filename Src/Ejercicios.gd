extends Node

onready var audioplayer = $AudioPlayer
onready var afinador = $Afinador
onready var player = $Player

func playMusic():
	player.initExercise()
	afinador.stopTuning()

func startTuning():
	audioplayer.stop()
	player.initExercise()
	afinador.startTuning()

func stop():
	player.finishExersice()
