extends Node

var test = "text"
var notes_ranges = {}
var melody_translation = {}
var min_frec = 58
var max_frec = 600

func _ready():
	notes_ranges = {
		"Si1":{"min_frec":58,   "max_frec":61},
		"Do2":{"min_frec":62,   "max_frec":67},
		"Re2":{"min_frec":70,   "max_frec":75},
		"Mi2":{"min_frec":80,   "max_frec":84},
		"Fa2":{"min_frec":85.5, "max_frec":89},
		"Sol2":{"min_frec":95,  "max_frec":101},
		"La2":{"min_frec":107,  "max_frec":113},
		"Si2":{"min_frec":121,  "max_frec":126},
		"Do3":{"min_frec":129.2,"max_frec":132},
		"Re3":{"min_frec":142,  "max_frec":150},
		"Mi3":{"min_frec":160,  "max_frec":168},
		"Fa3":{"min_frec":173,  "max_frec":176},
		"Sol3":{"min_frec":192, "max_frec":200},
		"La3":{"min_frec":217,  "max_frec":223},
		"Si3":{"min_frec":245,  "max_frec":250},
		"Do4":{"min_frec":258,  "max_frec":265},
		"Re4":{"min_frec":290,  "max_frec":296.5},
		"Mi4":{"min_frec":326,  "max_frec":333},
		"Fa4":{"min_frec":346,  "max_frec":353},
		"Sol4":{"min_frec":388, "max_frec":394},
		"La4":{"min_frec":437,  "max_frec":443},
		"Si4":{"min_frec":490,  "max_frec":497},
		"Do5":{"min_frec":520,  "max_frec":527},
		"Re5":{"min_frec":540,  "max_frec":600}
	}
	melody_translation = {
		"G4":"Sol4",
		"E4":"Mi4",
		"C4":"Do4",
		"G3":"Sol3",
		"E3":"Mi3",
		"C3":"Do3",
		"G2":"Sol2",
	}
	
func get_by_position(index):
	return notes_ranges.keys()[index]

func return_note(note):
	return NotesManager.notes_ranges.keys().find(note)

func get_max_frec(note):
	return NotesManager.notes_ranges[note]["max_frec"]
	
func get_min_frec(note):
	return NotesManager.notes_ranges[note]["min_frec"]

func getRandomNote():
	randomize()
	return notes_ranges.keys()[rand_range(1, 22)]

func getMusicSheetIndex(key):
	return melody_translation.keys().find(key)

func translate(note):
	return melody_translation[note]
