extends Node

var spectrum 
enum relacion {GRAVE = -1, AFINADA = 0, AGUDA = 1}

func _ready():
	spectrum = AudioServer.get_bus_effect_instance(1,0)

func tuner(nota):
	var notaActual = NotesManager.notes_ranges[nota]
	var NoteIndex = NotesManager.return_note(nota)
	var notaAnterior= NotesManager.get_by_position(NoteIndex-1)
	var notaSiguiente = NotesManager.get_by_position(NoteIndex+1)
	var prev_max_frec = NotesManager.get_max_frec(notaAnterior)
	var next_min_frec = NotesManager.get_min_frec(notaSiguiente)
	var amplitudGrave = spectrum.get_magnitude_for_frequency_range(NotesManager.min_frec, prev_max_frec).length()
	var amplitudNotaActual = spectrum.get_magnitude_for_frequency_range(notaActual["min_frec"], notaActual["max_frec"]).length()
	var amplitudAguda = spectrum.get_magnitude_for_frequency_range(next_min_frec, NotesManager.max_frec).length()
	if((amplitudAguda > amplitudNotaActual) && (amplitudAguda >amplitudGrave)):
		return relacion.AGUDA
	if((amplitudNotaActual > amplitudAguda) && (amplitudNotaActual >amplitudGrave)):
		return relacion.AFINADA
	if((amplitudGrave > amplitudAguda) && (amplitudGrave > amplitudNotaActual)):
		return relacion.GRAVE
