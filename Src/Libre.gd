extends Node

var nota 
onready var notaActual = $NotaActual
onready var afinador = $Afinador
onready var notaGrave = get_node("NotaActual/notaGrave")
onready var notaAguda = get_node("NotaActual/notaAguda")
onready var good = get_node("NotaActual/good")
onready var excellent = get_node("NotaActual/Exelent")
onready var progreso = get_node("NotaActual/Progreso")
onready var botonAyuda = get_node("BotonAyuda")
enum relacion {GRAVE = -1, AFINADA = 0, AGUDA = 1}
var segundosAfinando = 0
var tiempoDeseado = 2.5
var afinacionTerminada = false
signal changeNote(note)

func _ready():
	changeActualNote("Fa4")


func progress():
	if(!afinacionTerminada):
		if(segundosAfinando > tiempoDeseado):
			excellent.show()
			good.hide()
			finish()
		else:
			progreso.angle_to = (segundosAfinando * 360) / tiempoDeseado
			progreso.update()
			
func resetArrows():
	notaAguda.hide()
	notaGrave.hide()
	good.hide()

func _on_Button_pressed():
	play_note()

func _on_Button_reset():
	stop_note()
	excellent.hide()
	begin()
	segundosAfinando = 0

func begin():
	afinacionTerminada = false
	afinador.finish = false
	
func finish():
	afinacionTerminada = true
	afinador.finish = true

func _on_HSlider_value_changed(value):
	stop_note()
	changeActualNote(NotesManager.get_by_position(value))

func stop_note():
	var audioAyuda = $AudioAyuda
	if(audioAyuda.is_playing()):
		botonAyuda.text = "Reproducir Ayuda"
		audioAyuda.stop()

func play_note():
	resetArrows()
	var audioAyuda = $AudioAyuda
	if(!audioAyuda.is_playing()):
		finish()
		botonAyuda.text = "Parar Ayuda"
		var audio_file = "res://Notas/Tonos_puros/"+nota+".ogg"
		var sfx = load(audio_file) 
		audioAyuda.stream = sfx
		audioAyuda.play()
	else:
		begin()
		stop_note()

func _on_Random_pressed():
	changeActualNote(NotesManager.getRandomNote())
	_on_Button_reset()

func changeActualNote(newNote):
	afinador.finish = false
	afinador.nota = newNote
	$HSlider.value = NotesManager.return_note(newNote)
	nota = newNote
	notaActual.text = nota
	emit_signal("changeNote", nota)

func _on_Afinador_correctTuning(tiempoAfinado, delta):
	resetArrows()
	segundosAfinando += delta
	good.show()
	progress()
	
func _on_Afinador_incorrectTuning(tuning):
	resetArrows()
	segundosAfinando = 0
	if(tuning == relacion.GRAVE):
		notaGrave.show()
	if(tuning== relacion.AGUDA):
		notaAguda.show()
	progress()
